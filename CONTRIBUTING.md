# Guide de contribution

Déjà merci, on adore les contributions.

## Structure du projet

Le dossier src contient les sources.

blablbla

## Comment contribuer

La première chose est de créer un fork (cliquer sur le bouton fork)

Ensuite cloner ce dépot

```
git clone https://gitlab.com/morgan.guimard/gitna01.git
```

Vous placer dans le dossier

```
cd gitna01
```

Ajouter votre fork à la liste des remotes


```
git remote add mon_fork https://gitlab.com/VOUS/gitna01.git
```

Créez votre branche avec les conventions de nommage suivantes

```
# numéro_issue-titre (exemple: 123-Ajout-image-chaton)

git checkout -b 123-Ajout-image-chaton
```

Work, work, work...

Commitez les changements

```
git add --all
git commit -m "Commentaire avec numéro issue: #123"
```

Publiez vos modification dans votre fork

```
git push mon_fork 123-Ajout-image-chaton
```

Faire ensuite une Merge request (le lien est dans le retour de git push), ou bien [cliquez ici](https://gitlab.com/morgan.guimard/gitna01/-/merge_requests/new)


Partez ensuite sur un autre développement

```
git fetch origin -p
git checkout main
git pull
```

Reprendre les étapes à partir de la création d'une nouvelle branche














